package com.meow.thaithien.interactivebook;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Thien on 6/30/2015.
 */
public class CustomAdapter extends ArrayAdapter<BookData> {
    private Context context;
    private int resource;
    private ArrayList<BookData> objects;

    //constructor
    public CustomAdapter(Context context, int resource, ArrayList<BookData> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View Item_view = convertView;
        if (Item_view==null)
            Item_view = new CustomViewGroup(getContext());

        //book data contain cover and title
        BookData myData = objects.get(position);

        //bind view
        TextView Title = ((CustomViewGroup)Item_view).TitleContent;
        ImageView Cover = ((CustomViewGroup)Item_view).CoverContent;

        //Set title and cover
        Title.setText(myData.getTitle());
        Cover.setImageDrawable(myData.getCover());

        return Item_view;//return

    }
}
