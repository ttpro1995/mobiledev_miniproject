package com.meow.thaithien.interactivebook;

import android.content.SharedPreferences;

/**
 * Created by Thien on 6/30/2015.
 */
public class LevelSystem {
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private String exp_prefs_key;


    private int level;
    private int exp;

    public LevelSystem(SharedPreferences prefs, SharedPreferences.Editor editor, String exp_prefs_key) {
        this.prefs = prefs;
        this.editor = editor;
        this.exp_prefs_key = exp_prefs_key;
        LoadPrefs();
    }

    public int getLevel(){
        level = exp/10+1;
        return level;
    }

    public int getExp(){
        level = exp/10;
        return exp;
    }

    public void increaseEXP(){
        exp = exp+5;
        getLevel();
        StorePrefs();
    }

    private void LoadPrefs(){
       exp =  prefs.getInt(exp_prefs_key,0);
    }

    private void StorePrefs(){
        editor.remove(exp_prefs_key);
        editor.commit();
        editor.putInt(exp_prefs_key,exp);
        editor.commit();
    }
}
