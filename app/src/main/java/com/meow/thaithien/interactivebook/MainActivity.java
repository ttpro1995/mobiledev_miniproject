package com.meow.thaithien.interactivebook;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;


public class MainActivity extends ActionBarActivity {

    ImageView imageView;
    ImageButton startReadingButton;
    ImageButton BonusButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.meow_image);
        startReadingButton = (ImageButton) findViewById(R.id.StartReading_button_id);
        BonusButton = (ImageButton) findViewById(R.id.Bonus_button_id);


        startReadingButton.setImageDrawable(getResources().getDrawable(R.drawable.start_reading));
        BonusButton.setImageDrawable(getResources().getDrawable(R.drawable.bonus));
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.pusheen));
        startReadingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BookChooser.class);
                startActivity(intent);
            }
        });

        BonusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BonusActivity.class);
                startActivity(intent);
            }
        });


//        Intent intent = new Intent(MainActivity.this,Quiz.class);
//        startActivity(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();
        new LoadLayoutBackground().execute();
    }

    @Override
    protected void onPause() {
        super.onPause();
        UnloadBackground();
    }



    private class LoadLayoutBackground extends AsyncTask<Void,Void,Drawable>
    {
        @Override
        protected Drawable doInBackground(Void... params) {
            BackGroundSingleton backgroundSingleton = BackGroundSingleton.getInstance(MainActivity.this);
            Drawable image = backgroundSingleton.getBackground();
            return image;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            super.onPostExecute(drawable);
            LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_main_linearlayout_id);
            linearLayout.setBackground(drawable);
        }
    }

    private void UnloadBackground()
    {
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_main_linearlayout_id);
        linearLayout.setBackground(null);
    }

}
