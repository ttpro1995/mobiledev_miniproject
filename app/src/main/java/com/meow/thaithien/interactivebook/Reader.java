package com.meow.thaithien.interactivebook;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.widget.DrawerLayout;

import android.os.Bundle;

import android.support.v7.app.ActionBarActivity;
import android.text.Spannable;
import android.text.method.MovementMethod;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toolbar;


import java.util.ArrayList;
import java.util.prefs.Preferences;


public class Reader extends ActionBarActivity {

    String CURRENT_PAGE_KEY="current_page_key";

    //view
    DrawerLayout drawerLayout;
    ImageView imageView;
    TextView storyTextView;
    TextView pagenumTextView;
    Button nextButton;
    Button prevButton;
    Button quizButton;
    ListView drawerListView;

    //prefs
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    /*get extra from intent
    *  story of book
    *  all pic of book
    *  page limit of book
    * */

    //
    int book_code;
    int page_num;
    int page_limit=1;
    String Book_title=null;

    String[] Story;
    TypedArray All_Pic;
    ArrayList<String> PageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);

        //view
        imageView = (ImageView)findViewById(R.id.imageView_id);
        storyTextView = (TextView) findViewById(R.id.story_id);
        pagenumTextView = (TextView) findViewById(R.id.PageNum_id);
        drawerListView = (ListView) findViewById(R.id.DrawerListView);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        nextButton= (Button) findViewById(R.id.next_button_id);
        prevButton = (Button) findViewById(R.id.prev_button_id);
        quizButton = (Button) findViewById(R.id.quiz_button_id);


        //init prefs
        prefs= PreferenceManager.getDefaultSharedPreferences(Reader.this);
        editor = prefs.edit();

        //init logic
        LoadBook();
        CreatePageList();
        drawerLayout.openDrawer(drawerListView);


        // set listener
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToNextPage();
            }
        });

        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToPrevPage();
            }
        });

        quizButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToQuiz();
            }
        });

        drawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                page_num=position;
                LoadPage();
                drawerLayout.closeDrawer(drawerListView);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new LoadLayoutBackground().execute();
       RestoreCurrentPage();
        LoadPage();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SaveCurrentPage();
        UnloadBackground();
    }

    private void SaveCurrentPage(){
        String[] page_key_all = getResources().getStringArray(R.array.prefs_current_page);
        String current_page_key = page_key_all[book_code];
        editor.remove(current_page_key);
        editor.commit();
        editor.putInt(current_page_key, page_num);
        editor.commit();
    }

    private void RestoreCurrentPage(){
        String[] page_key_all = getResources().getStringArray(R.array.prefs_current_page);
        String current_page_key = page_key_all[book_code];
        page_num = prefs.getInt(current_page_key,0);
    }

    private void GoToNextPage(){
        page_num++;
        if (page_num <= page_limit)
            LoadPage();
        else page_num--;
    }

    private void GoToPrevPage(){
        page_num--;
        if (page_num>=0)
            LoadPage();
        else {
            page_num++;
            NavUtils.navigateUpFromSameTask(Reader.this);
        }
    }

    private void LoadPage()
    {
        if (page_num == 0)
        {pagenumTextView.setText(""); }
        else
        pagenumTextView.setText(Integer.toString(page_num));
        storyTextView.setText(Story[page_num]);
        Drawable tmp = All_Pic.getDrawable(page_num);
        imageView.setImageDrawable(tmp);
        if (page_num==page_limit)
        {
            nextButton.setVisibility(View.GONE);
            quizButton.setVisibility(View.VISIBLE);
        }
        else
        {
            nextButton.setVisibility(View.VISIBLE);
            quizButton.setVisibility(View.GONE);
        }
    }

    private void LoadBook(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        book_code = bundle.getInt(getResources().getString(R.string.extra_book_code_key));

        int[] book_page_limit = getResources().getIntArray(R.array.book_page_limit_code);
        page_limit = book_page_limit[book_code];
        String[] all_title = getResources().getStringArray(R.array.book_list);
        Book_title = all_title[book_code];
        Reader.this.setTitle(Book_title);
        if (book_code == 0)
        {
            Story = getResources().getStringArray(R.array.cinderella);
            All_Pic = getResources().obtainTypedArray(R.array.cinderella_drawable);
        }
        if (book_code==1)
        {
            Story = getResources().getStringArray(R.array.firebird);
            All_Pic = getResources().obtainTypedArray(R.array.firebird_drawable);
        }
        if (book_code==2)
        {
            Story = getResources().getStringArray(R.array.beach);
            All_Pic = getResources().obtainTypedArray(R.array.beach_drawable);
        }
        if (book_code==3)
        {
            Story = getResources().getStringArray(R.array.solar);
            All_Pic = getResources().obtainTypedArray(R.array.solar_drawable);
        }
        if (book_code==4)
        {
            Story = getResources().getStringArray(R.array.arabian);
            All_Pic = getResources().obtainTypedArray(R.array.arabian_drawable);
        }

    }

    private void CreatePageList(){
        PageList = new ArrayList<String>();
        String tmp_title = "Title";
        PageList.add(tmp_title);
        for (int i=1;i<=page_limit;i++)
        {
            String tmp = "Page "+Integer.toString(i);
            PageList.add(tmp);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Reader.this,R.layout.single_line_listview_item,R.id.list_item,PageList);
        drawerListView.setAdapter(adapter);
    }

    private void GoToQuiz(){
        Intent intent = new Intent(Reader.this,Quiz.class);
        intent.putExtra(getResources().getString(R.string.extra_book_code_key),book_code);
        //clear current quiz
        String[] quiz_key_all = getResources().getStringArray(R.array.prefs_current_quiz);
        String current_page_key = quiz_key_all[book_code];
        editor.remove(current_page_key);
        editor.commit();

        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reader, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.open_page_chooser) {
            drawerLayout.openDrawer(drawerListView);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class LoadLayoutBackground extends AsyncTask<Void,Void,Drawable>
    {
        @Override
        protected Drawable doInBackground(Void... params) {
            BackGroundSingleton backgroundSingleton = BackGroundSingleton.getInstance(Reader.this);
            Drawable image = backgroundSingleton.getBackground();
            return image;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            super.onPostExecute(drawable);
            LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_reader_linearlayout_id);
            linearLayout.setBackground(drawable);
        }
    }

    private void UnloadBackground()
    {
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_reader_linearlayout_id);
        linearLayout.setBackground(null);
    }
}
