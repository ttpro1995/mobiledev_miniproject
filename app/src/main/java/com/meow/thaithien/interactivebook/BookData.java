package com.meow.thaithien.interactivebook;

import android.graphics.drawable.Drawable;

/**
 * Created by Thien on 6/30/2015.
 */
public class BookData {
    private String Title;
    private Drawable Cover;


    public BookData(Drawable cover, String title) {
        Cover = cover;
        Title = title;
    }

    //default constructor
    public BookData() {
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Drawable getCover() {
        return Cover;
    }

    public void setCover(Drawable cover) {
        Cover = cover;
    }
}
