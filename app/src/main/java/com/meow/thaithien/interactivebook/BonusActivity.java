package com.meow.thaithien.interactivebook;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


public class BonusActivity extends ActionBarActivity {

    TextView levelTextView;
    TextView expTextView;

    //prefs
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String exp_key;

    //level
    LevelSystem levelSystem;

    //bonus button
    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bonus);
        BonusActivity.this.setTitle("Bonus");

        //view
        levelTextView = (TextView) findViewById(R.id.level_textview_id);
        expTextView = (TextView) findViewById(R.id.exp_textview_id);
        button1 = (Button) findViewById(R.id.bonus1_button_id);
        button2 = (Button) findViewById(R.id.bonus2_button_id);
        button3 = (Button) findViewById(R.id.bonus3_button_id);
        button4 = (Button) findViewById(R.id.bonus4_button_id);
        button5 = (Button) findViewById(R.id.bonus5_button_id);

        button1.setText("Cookies - A how to ");
        button2.setText("Solving the Mystery: a cupboard ");
        button3.setText("Pizza - A how to");
        button4.setText("Solving the Mystery: who ate the cookies");
        button5.setText("How to beat the summer heat");

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToBonus(0);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToBonus(1);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToBonus(2);
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToBonus(3);
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToBonus(4);
            }
        });


        //init prefs
        prefs= PreferenceManager.getDefaultSharedPreferences(BonusActivity.this);
        editor = prefs.edit();
        exp_key = getResources().getString(R.string.prefs_exp_key);
        levelSystem = new LevelSystem(prefs,editor,exp_key);



    }

    @Override
    protected void onResume() {
        super.onResume();
        new LoadLayoutBackground().execute();
        UpdateLevel();
        UpdateEXP();
        UnlockBonus();
    }

    private void GoToBonus(int code)
    {
        String bonus_code_key = getResources().getString(R.string.extra_bonus_code_key);
        Intent intent = new Intent(BonusActivity.this,BonusPlayer.class);
        intent.putExtra(bonus_code_key, code);
        startActivity(intent);
    }

    private void UpdateLevel(){
        int level = levelSystem.getLevel();
        levelTextView.setText("Level : " + Integer.toString(level));
    }
    private void UpdateEXP(){
        int exp = levelSystem.getExp();
        int max_exp = levelSystem.getLevel()*10;
        expTextView.setText("EXP : " + exp + "/" + max_exp);
    }

    private void UnlockBonus(){
        int level=levelSystem.getLevel();
        if (level>=3)
        {
            button1.setVisibility(View.VISIBLE);
        }
        if (level>=6)
        {
            button2.setVisibility(View.VISIBLE);
        }
        if (level>=9)
        {
            button3.setVisibility(View.VISIBLE);
        }
        if (level>=12)
        {
            button4.setVisibility(View.VISIBLE);
        }
        if (level>=15)
        {
            button5.setVisibility(View.VISIBLE);
        }
    }




    @Override
    protected void onPause() {
        super.onPause();
        UnloadBackground();
    }

    private class LoadLayoutBackground extends AsyncTask<Void,Void,Drawable>
    {
        @Override
        protected Drawable doInBackground(Void... params) {
            BackGroundSingleton backgroundSingleton = BackGroundSingleton.getInstance(BonusActivity.this);
            Drawable image = backgroundSingleton.getBackground();
            return image;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            super.onPostExecute(drawable);
            LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_bonus_linearlayout_id);
            linearLayout.setBackground(drawable);
        }
    }

    private void UnloadBackground()
    {
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_bonus_linearlayout_id);
        linearLayout.setBackground(null);
    }
}
