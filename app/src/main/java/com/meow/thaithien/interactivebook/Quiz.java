package com.meow.thaithien.interactivebook;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class Quiz extends ActionBarActivity {

    ImageView imageView;
    TextView quiz_textView;
    RadioGroup radioGroup;
    RadioButton radioButton1;
    RadioButton radioButton2;
    RadioButton radioButton3;
    Button submitButton;
    Button nextButton;
    Button doneButton;
    Button helpButton;
    MediaPlayer meow_sound1;

    int Book_code ; // get from intent
    //resource array of book
    TypedArray quiz_image_list;
    String[] quiz_list;
    String[] radio1_list;
    String[] radio2_list;
    String[] radio3_list;
    int[] Answer_list;

    //current quiz
    int quiz_num=0;
    int quiz_limit=2;//3 quiz total

    //prefs
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String prefs_exp_key;

    //level
    LevelSystem levelSystem;
    TextView levelTextView;
    TextView expTextView;

    //pusheen
    Drawable pusheen_love;
    Drawable pusheen_cry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        //view
        imageView = (ImageView) findViewById(R.id.quiz_imageView_id);
        quiz_textView = (TextView) findViewById(R.id.quiz_textView_id);
        radioGroup = (RadioGroup) findViewById(R.id.quiz_radioGroup_id);
        radioButton1 = (RadioButton) findViewById(R.id.radio1_id);
        radioButton2 = (RadioButton) findViewById(R.id.radio2_id);
        radioButton3 = (RadioButton) findViewById(R.id.radio3_id);
        submitButton = (Button) findViewById(R.id.submit_button_id);
        nextButton = (Button) findViewById(R.id.quiz_next_button_id);
        doneButton = (Button)findViewById(R.id.quiz_done_button_id);
        doneButton.setVisibility(View.GONE);
        levelTextView = (TextView) findViewById(R.id.level_quiz_textview_id);
        expTextView = (TextView) findViewById(R.id.exp_quiz_textview_id);
        helpButton = (Button) findViewById(R.id.help_button_id);


        //init
        LoadBookQuiz();
        LoadQuiz();
        pusheen_cry = getResources().getDrawable(R.drawable.pusheen_cry);
        pusheen_love = getResources().getDrawable(R.drawable.pusheen_love);

        //init prefs
        prefs= PreferenceManager.getDefaultSharedPreferences(Quiz.this);
        editor = prefs.edit();
        prefs_exp_key = getResources().getString(R.string.prefs_exp_key);
        levelSystem = new LevelSystem(prefs,editor,prefs_exp_key);

        //meow
        meow_sound1 = MediaPlayer.create(Quiz.this, R.raw.cat_meow);

        //
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubmitAnswer();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NextQuiz();
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Help();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new LoadLayoutBackground().execute();
        RestoreCurrentQuiz();
        LoadQuiz();
        UpdateLevel();
        UpdateEXP();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SaveCurrentQuiz();
        UnloadBackground();
    }

    private void UpdateLevel(){
        int level = levelSystem.getLevel();
        levelTextView.setText("Level : " + Integer.toString(level));
    }
    private void UpdateEXP(){
        int exp = levelSystem.getExp();
        int max_exp = levelSystem.getLevel()*10;
        expTextView.setText("EXP : " + exp + "/" + max_exp);
    }

    private void LoadBookQuiz(){
        Intent intent = getIntent();
        Bundle data = intent.getExtras();
        Book_code = data.getInt(getResources().getString(R.string.extra_book_code_key));

        if (Book_code==0){
            quiz_image_list = getResources().obtainTypedArray(R.array.cinderella_quiz_pic_list);
            quiz_list = getResources().getStringArray(R.array.cinderella_quiz_list);
            radio1_list = getResources().getStringArray(R.array.cinderella_quiz_radio1_list);
            radio2_list = getResources().getStringArray(R.array.cinderella_quiz_radio2_list);
            radio3_list = getResources().getStringArray(R.array.cinderella_quiz_radio3_list);
            Answer_list = getResources().getIntArray(R.array.cinderella_quiz_anwser_list);
        }
        if (Book_code==1){
            quiz_image_list = getResources().obtainTypedArray(R.array.firebird_quiz_pic_list);
            quiz_list = getResources().getStringArray(R.array.firebird_quiz_list);
            radio1_list = getResources().getStringArray(R.array.firebird_quiz_radio1_list);
            radio2_list = getResources().getStringArray(R.array.firebird_quiz_radio2_list);
            radio3_list = getResources().getStringArray(R.array.firebird_quiz_radio3_list);
            Answer_list = getResources().getIntArray(R.array.firebird_quiz_anwser_list);
        }

        if (Book_code==2){
            quiz_image_list = getResources().obtainTypedArray(R.array.beach_quiz_pic_list);
            quiz_list = getResources().getStringArray(R.array.beach_quiz_list);
            radio1_list = getResources().getStringArray(R.array.beach_quiz_radio1_list);
            radio2_list = getResources().getStringArray(R.array.beach_quiz_radio2_list);
            radio3_list = getResources().getStringArray(R.array.beach_quiz_radio3_list);
            Answer_list = getResources().getIntArray(R.array.beach_quiz_anwser_list);
        }

        if (Book_code==3){
            quiz_image_list = getResources().obtainTypedArray(R.array.solar_quiz_pic_list);
            quiz_list = getResources().getStringArray(R.array.solar_quiz_list);
            radio1_list = getResources().getStringArray(R.array.solar_quiz_radio1_list);
            radio2_list = getResources().getStringArray(R.array.solar_quiz_radio2_list);
            radio3_list = getResources().getStringArray(R.array.solar_quiz_radio3_list);
            Answer_list = getResources().getIntArray(R.array.solar_quiz_anwser_list);
        }

        if (Book_code==4){
            quiz_image_list = getResources().obtainTypedArray(R.array.arabian_quiz_pic_list);
            quiz_list = getResources().getStringArray(R.array.arabian_quiz_list);
            radio1_list = getResources().getStringArray(R.array.arabian_quiz_radio1_list);
            radio2_list = getResources().getStringArray(R.array.arabian_quiz_radio2_list);
            radio3_list = getResources().getStringArray(R.array.arabian_quiz_radio3_list);
            Answer_list = getResources().getIntArray(R.array.arabian_quiz_anwser_list);
        }

    }

    private void LoadQuiz(){
        imageView.setImageDrawable(quiz_image_list.getDrawable(quiz_num));
        quiz_textView.setText(quiz_list[quiz_num]);
        radioButton1.setText(radio1_list[quiz_num]);
        radioButton2.setText(radio2_list[quiz_num]);
        radioButton3.setText(radio3_list[quiz_num]);

        nextButton.setVisibility(View.GONE);
        submitButton.setVisibility(View.VISIBLE);
        radioGroup.clearCheck();


    }

    private void NextQuiz(){
        quiz_num++;
        if (quiz_num<=quiz_limit)
            LoadQuiz();
        else
            quiz_num--;
    }

    private boolean CheckAnswer(){
        int answer_id = radioGroup.getCheckedRadioButtonId();
        if (answer_id==R.id.radio1_id&&Answer_list[quiz_num]==1)
        {
            return true;
        }
        if (answer_id==R.id.radio2_id&&Answer_list[quiz_num]==2)
        {
            return true;
        }
        if (answer_id==R.id.radio3_id&&Answer_list[quiz_num]==3)
        {
            return true;
        }
        return false;
    }

    private void Help(){
        int anwser = Answer_list[quiz_num];
        String tmp = "Correct answer is ";
        if (Answer_list[quiz_num]==1)
        {
            tmp=tmp+radioButton1.getText().toString();
        }
        if (Answer_list[quiz_num]==2)
        {
            tmp=tmp+radioButton2.getText().toString();
        }
        if (Answer_list[quiz_num]==3)
        {
            tmp=tmp+radioButton3.getText().toString();
        }
        Toast.makeText(Quiz.this,tmp,Toast.LENGTH_SHORT).show();
        imageView.setImageDrawable(pusheen_cry);
        submitButton.setVisibility(View.GONE);
        nextButton.setVisibility(View.VISIBLE);
        if (quiz_num==quiz_limit)
        {
            nextButton.setVisibility(View.GONE);
            doneButton.setVisibility(View.VISIBLE);
        }
    }

    private void SubmitAnswer(){
        boolean isCorrect = CheckAnswer();
        if (isCorrect)
        {
            levelSystem.increaseEXP();
            UpdateLevel();
            UpdateEXP();
            imageView.setImageDrawable(pusheen_love);
            String tmp_toast;
            int level = levelSystem.getLevel();
            int exp = levelSystem.getExp();
            if ((level==3||level==6||level==9||level==12||level==15)&&(exp%10==0))
            {tmp_toast="Correct +5 EXP \n New bonus unlocked";}
            else
            {
                tmp_toast="Correct +5 EXP";
            }
            meow_sound1.start();
            Toast.makeText(Quiz.this,tmp_toast,Toast.LENGTH_SHORT).show();

        }
        else
        {
            imageView.setImageDrawable(pusheen_cry);
            Toast.makeText(Quiz.this,"Incorrect",Toast.LENGTH_SHORT).show();
        }
        submitButton.setVisibility(View.GONE);
        nextButton.setVisibility(View.VISIBLE);
        if (quiz_num==quiz_limit)
        {
            nextButton.setVisibility(View.GONE);
            doneButton.setVisibility(View.VISIBLE);
        }
    }

    private void SaveCurrentQuiz(){
        String[] quiz_key_all = getResources().getStringArray(R.array.prefs_current_quiz);
        String current_page_key = quiz_key_all[Book_code];
        editor.remove(current_page_key);
        editor.commit();
        editor.putInt(current_page_key, quiz_num);
        editor.commit();
    }

    private void RestoreCurrentQuiz(){
        String[] quiz_key_all = getResources().getStringArray(R.array.prefs_current_quiz);
        String current_page_key = quiz_key_all[Book_code];
        quiz_num = prefs.getInt(current_page_key,0);
    }

    private class LoadLayoutBackground extends AsyncTask<Void,Void,Drawable>
    {
        @Override
        protected Drawable doInBackground(Void... params) {
            BackGroundSingleton backgroundSingleton = BackGroundSingleton.getInstance(Quiz.this);
            Drawable image = backgroundSingleton.getBackground();
            return image;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            super.onPostExecute(drawable);
            LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_quiz_linearlayout_id);
            linearLayout.setBackground(drawable);
        }
    }

    private void UnloadBackground()
    {
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_quiz_linearlayout_id);
        linearLayout.setBackground(null);
    }
}
