package com.meow.thaithien.interactivebook;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.meow.thaithien.interactivebook.views.GifDecoderView;

import java.io.IOException;
import java.io.InputStream;


//Use code from http://droid-blog.net/2011/10/15/tutorial-how-to-play-animated-gifs-in-android-%E2%80%93-part-2/
// https://code.google.com/p/animated-gifs-in-android/
public class BonusPlayer extends Activity {

    String[] file_names_array;
    int bonus_code;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bonus_player);
        linearLayout = (LinearLayout) findViewById(R.id.bonus_linearlayout_id);


    }

    @Override
    protected void onResume() {
        super.onResume();
        new LoadLayoutBackground().execute();
        LoadBonusInfo();
        DisplayBonus();
    }

    @Override
    protected void onPause() {
        super.onPause();
        UnloadBackground();
    }

    private void LoadBonusInfo(){
        Intent intent = getIntent();
        Bundle data = intent.getExtras();
        bonus_code = data.getInt(getResources().getString(R.string.extra_bonus_code_key));

        if (bonus_code==0)
            file_names_array=getResources().getStringArray(R.array.cookies);
        if (bonus_code==1)
            file_names_array=getResources().getStringArray(R.array.cupboard);
        if (bonus_code==2)
            file_names_array=getResources().getStringArray(R.array.pizza);
        if (bonus_code==3)
            file_names_array=getResources().getStringArray(R.array.solving);
        if (bonus_code==4)
            file_names_array=getResources().getStringArray(R.array.summer);
    }

    private void DisplayBonus(){
        InputStream stream = null;
        for (int i=0;i<file_names_array.length;i++)
        {
            try {
                stream = getAssets().open(file_names_array[i]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            GifDecoderView view = new GifDecoderView(this, stream);
            linearLayout.addView(view);
        }
    }


    private class LoadLayoutBackground extends AsyncTask<Void,Void,Drawable>
    {
        @Override
        protected Drawable doInBackground(Void... params) {
            BackGroundSingleton backgroundSingleton = BackGroundSingleton.getInstance(BonusPlayer.this);
            Drawable image = backgroundSingleton.getBackground();
            return image;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            super.onPostExecute(drawable);
            LinearLayout linearLayout = (LinearLayout)findViewById(R.id.bonus_linearlayout_id);
            linearLayout.setBackground(drawable);
        }
    }

    private void UnloadBackground()
    {
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.bonus_linearlayout_id);
        linearLayout.setBackground(null);
    }
}
