package com.meow.thaithien.interactivebook;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.RequiresPermission;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;


public class BookChooser extends Activity {

    String [] Array_all_book;
    ListView all_book_listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_chooser);
        all_book_listview = (ListView) findViewById(R.id.BookChooserListView_id);
        CreateAllBookList();
        all_book_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pick_a_book(position);
            }
        });
    }

    private void CreateAllBookList(){
        Array_all_book = getResources().getStringArray(R.array.book_list);
        ArrayList<BookData> bookDatas = new ArrayList<BookData>();

        String[] title_list = getResources().getStringArray(R.array.book_list);
        TypedArray cover_list = getResources().obtainTypedArray(R.array.cover);
        for (int i=0;i<title_list.length;i++)
        {
            Drawable tmp_cover = cover_list.getDrawable(i);
            String tmp_title = title_list[i];
            BookData tmp = new BookData(tmp_cover,tmp_title);
            bookDatas.add(tmp);
        }

        CustomAdapter customAdapter = new CustomAdapter(BookChooser.this,R.layout.cover_title_list_item,bookDatas);
        all_book_listview.setAdapter(customAdapter);
    }

    private void Pick_a_book(int position)
    {
        String BOOK_CODE_KEY = getResources().getString(R.string.extra_book_code_key);
            Intent intent = new Intent(BookChooser.this, Reader.class);
            //put extra the position book code
            intent.putExtra(BOOK_CODE_KEY, position);
            startActivity(intent);

    }


    @Override
    protected void onResume() {
        super.onResume();
        new LoadLayoutBackground().execute();
    }

    @Override
    protected void onPause() {
        super.onPause();
        UnloadBackground();
    }

    private class LoadLayoutBackground extends AsyncTask<Void,Void,Drawable>
    {
        @Override
        protected Drawable doInBackground(Void... params) {
            BackGroundSingleton backgroundSingleton = BackGroundSingleton.getInstance(BookChooser.this);
            Drawable image = backgroundSingleton.getBackground();
            return image;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            super.onPostExecute(drawable);
            LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_chooser_linearlayout_id);
            linearLayout.setBackground(drawable);
        }
    }

    private void UnloadBackground()
    {
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_chooser_linearlayout_id);
        linearLayout.setBackground(null);
    }

}
